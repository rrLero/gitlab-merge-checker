(function () {

    const setMergeCheckerObj =
        (currObj = {switched: false, isMergeButton: false, isButtonDisabled: false}) => (merge = {}) => {
        currObj = {...currObj, ...merge};
        return currObj;
    };

    const reg = /(^release\/)/;
    const el = [...document.querySelectorAll("strong")]
        .filter(a => a.textContent.includes("Request to merge"))[0];
    const button = [...document.querySelectorAll("button")]
        .filter(a => a.textContent.includes("Merge"))[0];
    if (!el || !button) {
        return
    }

    const objModifier = setMergeCheckerObj({isMergeButton: true});

    const arrA = [...el.querySelectorAll("a")].map(a => a.textContent.trim());
    if (arrA.length !== 2) {
    	return
	}
    const sourceBranch = arrA[0].match(reg);
    const targetBranch = arrA[1].match(reg);

    if (sourceBranch) {
        if (targetBranch) {
            objModifier({
                isMergeButton: true,
                isButtonDisabled: false,
                switched: true
            });
        } else {
            objModifier({
                isMergeButton: true,
                isButtonDisabled: true,
                switched: true
            });
            button.setAttribute("disabled", "disabled");
        }
	}

    chrome.runtime.onMessage.addListener(function (msg, sender, response) {
        if ((msg.from === 'popup') && (msg.subject === 'DOMInfo')) {
            response(objModifier());
        }
        if ((msg.from === 'popup') && (msg.subject === 'change')) {
            const {isButtonDisabled: currValue} = objModifier();
            response(objModifier({
                isButtonDisabled: !currValue
            }));
            currValue ? button.removeAttribute("disabled") : button.setAttribute("disabled", "disabled");
        }
    });
}());
