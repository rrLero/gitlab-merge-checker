(function () {
    setDOMInfo = (obj = {}, isEvent = true) => {
        const {isMergeButton, isButtonDisabled, switched} = obj;
        const switchEl = document.getElementById('switched');
        const isMergeButtonEl = document.getElementById('isMergeButton');
        const isButtonDisabledEl = document.getElementById('isButtonDisabled');

        switchEl.innerText = isButtonDisabled ? 'ON' : 'OFF';
        isMergeButtonEl.checked = isMergeButton;
        isButtonDisabledEl.checked = isButtonDisabled;

        isMergeButtonEl.getElementsByTagName('span')[0].innerText = `${!isMergeButton ? 'NOT FOUND' : 'FOUND'}`;
        isButtonDisabledEl.getElementsByTagName('span')[0].innerText = `${!isButtonDisabled ? 'ENABLED' : 'DISABLED'}`;

        if (!switched) {
            switchEl.parentNode.removeChild(switchEl);
            isButtonDisabledEl.parentNode.removeChild(isButtonDisabledEl);

        }
        isEvent && isMergeButton && switchEl.addEventListener('click', (event) => {
            chrome.tabs.query({
                active: true,
                currentWindow: true
            }, function (tabs) {
                chrome.tabs.sendMessage(
                    tabs[0].id,
                    {from: 'popup', subject: 'change'},
                    (obj) => {
                        setDOMInfo(obj, false)
                    }
                );
            });

        });
    };

    window.addEventListener('DOMContentLoaded', function () {
        chrome.tabs.query({
            active: true,
            currentWindow: true
        }, function (tabs) {
            chrome.tabs.sendMessage(
                tabs[0].id,
                {from: 'popup', subject: 'DOMInfo'},
                setDOMInfo);
        });
    });
})();