# GitLab Merge-Checker

*CHROME - extension*

## Description

This extension disables Merge-Button if SourceBranch contains 'release', but TargetBranch don't

## Setup Plugin

1. Download this repo to any local folder.
2. Enter to the chrome://extensions/
3. Switch on "Developer mode"
4. Click Load unpacked extension and select the directory in which your extension files live. If the extension is valid, it will be active straight away